import os
import subprocess
import sys
from threading import Thread
from flask import Flask
from PIL import Image
from pystray import Icon, MenuItem

app = Flask(__name__)


@app.route('/shutdown', methods=['GET'])
def shutdown():
    # Check the operating system
    if sys.platform.startswith('win'):
        os.system('shutdown /s /t 0')  # Shutdown Windows
    elif sys.platform.startswith('linux') or sys.platform.startswith('darwin'):
        subprocess.call(['shutdown', '-h', 'now'])  # Shutdown Linux or Mac
    else:
        return 'Unsupported operating system.'

    return 'Shutting down...'


def exit_action(icon, item):
    icon.stop()


def resource_path(relative_path):
    base_path = getattr(sys, '_MEIPASS', os.path.dirname(
        os.path.abspath(__file__)))
    return os.path.join(base_path, relative_path)


if __name__ == '__main__':
    # Create system tray icon
    image = Image.open(resource_path("icon.ico"))
    icon = Icon('Remote shudown PC', image)
    icon.menu = (
        MenuItem('Exit', exit_action),
    )

    # Start Flask app in the background
    def flask_thread():
        app.run()

    flask_thread = Thread(target=flask_thread)
    flask_thread.daemon = True
    flask_thread.start()

    # Run the system tray icon
    icon.run()
